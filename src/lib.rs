mod builder;
mod vm;

pub use vm::{RegisterVM, VMFunction, Frame, FrameValue, LazyCompiler, HCFFunction};
pub use builder::{FrameBuilder, VMBuilder, VMInstruction};

#[derive(Clone, Debug, PartialEq)]
pub enum VMError {
    InvalidInstruction(String),
    InvalidFunction(String),
    IllegalArgument(String),
    RuntimeError(String),
    StackUnderflow(String),
    LocalNotFound(String),
    ImmutableLocal(String),
    RegisterNotFound(String),
    InvalidFrame(String),
    LazyDisabled(String),
}

use num_enum::{FromPrimitive, IntoPrimitive};

#[derive(IntoPrimitive, FromPrimitive)]
#[repr(u8)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum Op {
    NOP, // increment pc by 1
    PAUS, // pause execution with register to check, r1
    LOAD, // load value
    LODB, // load bool register value
    LODI, // load index register value
    ADD,
    MOD,
    SUB,
    MUL,
    DIV,
    SHR,
    SHL,
    XOR,
    BAND,
    BOR,
    BXOR,
    LXOR,
    LAND,
    LOR,
    NOT,
    BNOT,
    NEG,
    REV,
    AND,
    OR,
    EQ,
    BEQ,
    IEQ,
    NEQ,
    BNEQ,
    INEQ,
    LT,
    LTE,
    GT,
    GTE,
    COPY, // r1 r2
    CIR, // ir1 ir2
    CBR, // br1 br2
    CALL, // frame#, 0 for tail recursion
    CALLR, // ir1, used to call a frame created by CFR
    CEQ, // br1 ir1 ir2 call if equal
    RET, // r1 r2, return from a frame (from => to)
    GLV, // r1 <name>, get local value
    MVL, // fr1 <name>, move local value from current frame to fr1
    CPL, // fr1 <name> , copy local value from current frame to fr1
    CPM, // fr1 <name>, copy local value from current frame to fr1, as mutable
    DLV, // fr1 <name>, delete local value from frame
    DFV, // <name>, delete local value from frame
    SLR, // r1 <name>, set local value from register
    SMR, // r1 <name>, set mutable local value from register
    SLF, // fr1 <name>, set local value from frame register (calling this variable will evaluate its frame)
    CFR, // r1 <len> [instructions], create & enter frame
    DFR, // frame#, delete frame
    DFI, // ir1, delete frame
    LZY, // call the lazy compiler
    PSH, // r1, push r1 to value_stack
    PSHV, // <value>, push <value> to value_stack
    POP, // r1, pop from value_stack and assign to r1
    /** Actors
    SND,
    RCV,
    PRC, // create process, does not start
    PRCR, // create process then run
    KPRC, // kill process
    RPRC, // run process
    SPRC, // sleep process
    */
    FN, // <command> <len> out, [args, in registers: r1, r2, r4, ...]
    IVF, // invalid frame, not used directly
    HCF,
    #[default]
    IVD,
}

#[cfg(test)]
mod tests {
    use small_len::{Length, SmallLen};
    use vm_value::length_to_bytes;
    use crate::builder::Register;
    use crate::vm::Frame;
    use super::*;
    use vm_value::VM;

    #[test]
    fn nop_works() {
        let mut vm: RegisterVM<i8> = VMBuilder::new()
            .add_nop_instruction()
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.pc(), 2);
    }

    #[test]
    fn load_works_zero_register() {
        let mut vm: RegisterVM<i8> = VMBuilder::new()
            .set_value(0.into(), 42)
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers.get(&0.into()), None);
    }

    #[test]
    fn copy_works() {
        let mut vm = VMBuilder::new()
            .set_value(1.into(), 42)
            .add_copy_instruction(1.into(), 88.into())
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers[&88.into()], 42);
    }

    struct TestJit {
        instructions: Vec<Vec<u8>>
    }

    impl LazyCompiler<i8> for TestJit {
        fn compile(&self, length: Length) -> Result<Frame<i8>, VMError> {
            let a = &self.instructions[length];
            let mut frame= Frame::new();
            frame.instructions = a.clone();
            Ok(frame)
        }
    }
    #[test]
    fn lazy_works() {
        let source = TestJit {
            instructions: vec![
                vec![Op::NOP as u8]
            ]
        };

        let mut vm: RegisterVM<i8> = VMBuilder::new()
            .add_lzy_instruction(0.into())
            .with_lazy_compiler(Box::new(source))
            .build();
        assert_eq!(vm.frames.len(), 1);
        vm.execute().expect("Run Failed");
        assert_eq!(vm.frames[1].instructions[0], Op::NOP as u8);
    }

    #[test]
    fn fn_works() {
        let mut vm: RegisterVM<i16> = VMBuilder::new()
            .set_value(257.into(), 42)
            .set_value(1.into(), 42)
            .add_fn_instruction(0.into(), vec![257.into(), 1.into()], 3.into())
            .add_function(move |_, args| {
                let a = args[0];
                let b = args[1];
                Ok(Some(a + b))
            })
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers[&3.into()], 84);
    }

    #[test]
    fn hcf_works() {
        let mut vm: RegisterVM<i8> = VMBuilder::new()
            .add_hcf_instruction()
            .with_hcf_function(move |vm| {
                vm.set_register_value(1.into(), i8::MAX);
                Ok(())
            })
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers[&1.into()], i8::MAX);
    }

    #[test]
    fn frames_work() {
        let mut vm: RegisterVM<i8> = VMBuilder::new()
            .enter_frame(Frame::new())
            .set_value(42.into(), 37)
            .exit_frame(42.into(), 4.into())
            .add_call_instruction(1.into())
            .add_dfr_instruction(1.into())
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers[&4.into()], 37);
        assert_eq!(vm.frames.len(), 2);
        assert_eq!(vm.frames[1].instructions, vec![Op::IVF as u8]);
    }

    #[test]
    fn dynamic_frames_work() {
        let mut cfr = vec![Op::SLR as u8];
        let register = 42.into();
        cfr.extend(length_to_bytes(&register));

        let bytes = "c".as_bytes();
        let len = bytes.small_len();
        cfr.extend(length_to_bytes(&len));
        cfr.extend(bytes);

        let mut vm: RegisterVM<i8> = VMBuilder::new()
            .set_value(register.into(), 78)
            .add_cfr_instruction(1.into(), cfr)
            .add_call_register_instruction(1.into())
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(*vm.frames[vm.fp].locals.get("c").expect("no value"), FrameValue::Let(78));
    }

    #[test]
    fn locals_work() {
        let mut vm: RegisterVM<i16> = VMBuilder::new()
            .set_value(1.into(), 231)
            .set_value(2.into(), 238)
            .add_slr_instruction(2.into(), "a".into())
            .add_slr_instruction(1.into(), "b".into())
            .add_glv_instruction(4.into(), "a".into())
            .add_glv_instruction(9.into(), "b".into())
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers[&4.into()], 238);
        assert_eq!(vm.registers[&9.into()], 231);
    }

    #[test]
    fn locals_mut_work() {
        let mut vm: RegisterVM<i16> = VMBuilder::new()
            .set_value(1.into(), 231)
            .set_value(2.into(), 31)
            .add_smr_instruction(2.into(), "a".into())
            .add_smr_instruction(1.into(), "b".into())
            .add_glv_instruction(4.into(), "a".into())
            .add_glv_instruction(9.into(), "b".into())
            .build();
        vm.execute().expect("Run Failed");
        assert_eq!(vm.registers[&4.into()], 31);
        assert_eq!(vm.registers[&9.into()], 231);
    }

    macro_rules! test_unary_op {
    ($($name:ident: $func:ident($op:tt)),+) => {
        $(
            #[test]
            fn $name() {
                let reg: usize = rand::random();
                let reg: Register = reg.into();
                let a: i8 = rand::random();
                let mut vm = VMBuilder::new()
                    .set_value(reg, a)
                    .$func(reg.clone())
                    .build();
                vm.execute().expect("run failed");
                assert_eq!(vm.registers[&reg], $op a);
            }
        )+
    };
}

    macro_rules! test_bin_op {
    ($($name:ident: $func:ident($op:tt)),+) => {
        $(
            #[test]
            fn $name() {
                let reg: usize = rand::random();
                let reg: Register = reg.into();
                let a: i8 = rand::random();
                let b: i8 = rand::random();
                let mut vm: RegisterVM<i16> = VMBuilder::new()
                    .set_value(reg, a as i16)
                    .set_value(2.into(), b as i16)
                    .$func(reg, 2.into())
                    .build();
                vm.execute().expect("run failed");
                let a = a as i16;
                let b = b as i16;
                assert_eq!(vm.registers[&reg], a $op b);
            }
        )+
    };
}

    test_unary_op!(
        not_works: add_not_instruction(!),
        neg_works: add_neg_instruction(-)
    );

    test_bin_op!(
        add_works: add_add_instruction(+),
        sub_works: add_sub_instruction(-),
        mul_works: add_mul_instruction(*),
        bitand_works: add_and_instruction(&),
        bitor_works: add_or_instruction(|),
        bitxor_works: add_xor_instruction(^)
    );
}
