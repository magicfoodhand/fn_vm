use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::mem;
use indexmap::IndexMap;
use small_len::{Len, Length, SmallLen};
use vm_value::{length_to_bytes, VMValue};
use crate::{FrameValue, Op, RegisterVM};
use crate::vm::{Frame, HCFFunction, LazyCompiler, VMFunction};

pub type Register = Length;

pub struct VMBuilder<T: VMValue<Op, Register, T>> {
    registers: HashMap<Length, T>,
    index_registers: HashMap<Length, Length>,
    bit_registers: HashMap<Length, bool>,
    frames: Vec<Frame<T>>,
    fp: usize,
    functions: Vec<VMFunction<T>>,
    hcf_trigger: Option<HCFFunction<T>>,
    lazy_compiler: Option<Box<dyn LazyCompiler<T>>>
}

impl <T: VMValue<Op, Register, T>> Debug for VMBuilder<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let lazy = match self.lazy_compiler {
            None => {
                "None"
            }
            Some(_) => "Some(fn(Length) => Result<Frame<dyn VMValue>, VMError>)"
        };
        write!(f, "VMBuilder(registers={:?}, index_registers={:?}, frames={:?}, fp={}, functions={:?}, hcf_trigger={:?}, lazy_compiler={})", self.registers, self.index_registers, self.frames, self.fp, self.functions, self.hcf_trigger, lazy)
    }
}


#[derive(Clone, Default, Debug)]
pub struct VMInstruction {
    pub op: Op,
    pub r1: Option<Register>,
    pub r2: Option<Register>,
    pub r3: Option<Register>,
}

impl VMInstruction {
    pub fn for_vm(&self) -> Vec<u8> {
        let this = self.clone();
        let mut result = vec![this.op as u8];
        if let Some(r1) = this.r1 {
            result.extend(length_to_bytes(&r1.into()))
        }
        if let Some(r2) = this.r2 {
            result.extend(length_to_bytes(&r2.into()))
        }
        if let Some(r3) = this.r3 {
            result.extend(length_to_bytes(&r3.into()))
        }
        result
    }
}

macro_rules! generate_zero_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self) -> &mut Self {
                self.add_zero_instruction($op)
            }
        )*
    };
}

macro_rules! generate_unary_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self, a: Register) -> &mut Self {
                self.add_unary_instruction($op, a)
            }
        )*
    };
}

macro_rules! generate_reg_value_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self, a: Register, v: T) -> &mut Self {
                self.add_unary_instruction($op, a).store_value(v)
            }
        )*
    };
}

macro_rules! generate_reg_str_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self, a: Register, v: String) -> &mut Self {
                self.add_unary_instruction($op, a).store_str(v)
            }
        )*
    };
}

macro_rules! generate_str_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self, v: String) -> &mut Self {
                self.add_zero_instruction($op).store_str(v)
            }
        )*
    };
}

macro_rules! generate_value_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self, v: T) -> &mut Self {
                self.add_zero_instruction($op).store_value(v)
            }
        )*
    };
}

macro_rules! generate_binary_instruction {
    ($($name:ident: $op:expr),*) => {
        $(
            pub fn $name(&mut self, a: Register, b: Register) -> &mut Self {
                self.add_binary_instruction($op, a, b)
            }
        )*
    };
}

macro_rules! common_instructions {
    () => {
        fn add_instruction(&mut self, instruction: VMInstruction) -> &mut Self {
            self.extend_instructions(instruction.for_vm())
        }

        fn add_zero_instruction(&mut self, op: Op) -> &mut Self {
            self.add_instruction(VMInstruction {
                op,
                r1: None,
                r2: None,
                r3: None,
            });
            self
        }

        fn add_unary_instruction(&mut self, op: Op, a: Register) -> &mut Self {
            self.add_instruction(VMInstruction {
                op,
                r1: Some(a),
                r2: None,
                r3: None,
            });
            self
        }

        fn add_binary_instruction(&mut self, op: Op, a: Register, b: Register) -> &mut Self {
            self.add_instruction(VMInstruction {
                op,
                r1: Some(a),
                r2: Some(b),
                r3: None,
            });
            self
        }

        pub fn add_fn_instruction(&mut self, function: Length, args: Vec<Register>, output: Register) -> &mut Self {
            let mut this = self.add_instruction(VMInstruction {
                op: Op::FN,
                r1: Some(function),
                r2: Some(args.length()),
                r3: Some(output),
            });
            for arg in args {
                this = this.store_register(arg)
            }
            self
        }

        pub fn add_cfr_instruction(&mut self, to: Register, bytes: Vec<u8>) -> &mut Self {
            self.add_unary_instruction(Op::CFR, to)
                .extend_instructions(length_to_bytes(&bytes.small_len()))
                .extend_instructions(bytes)
        }

        pub fn store_value(&mut self, value: T) -> &mut Self {
            let bytes = value.to_vm_bytes();
            let len = bytes.small_len();
            self.extend_instructions(length_to_bytes(&len));
            self.extend_instructions(bytes);
            self
        }

        pub fn store_str(&mut self, str: String) -> &mut Self {
            let bytes = str.as_bytes();
            let len: Length = bytes.len().into();
            self.extend_instructions(len.to_be_bytes());
            self.extend_instructions_raw(bytes);
            self
        }

        generate_zero_instruction!(
            add_nop_instruction: Op::NOP,
            add_hcf_instruction: Op::HCF,
            add_ivd_instruction: Op::IVD,
            add_ivf_instruction: Op::IVF
        );

        generate_reg_value_instruction!(
            add_load_instruction: Op::LOAD
        );

        generate_reg_str_instruction!(
            add_glv_instruction: Op::GLV,
            add_mvl_instruction: Op::MVL,
            add_cpl_instruction: Op::CPL,
            add_cpm_instruction: Op::CPM,
            add_slr_instruction: Op::SLR,
            add_smr_instruction: Op::SMR,
            add_dlv_instruction: Op::DLV
        );

        generate_str_instruction!(
            add_dfv_instruction: Op::DFV
        );

        generate_value_instruction!(
            add_pshv_instruction: Op::PSHV
        );

        generate_unary_instruction!(
            add_not_instruction: Op::NOT,
            add_rev_instruction: Op::REV,
            add_neg_instruction: Op::NEG,
            add_call_instruction: Op::CALL,
            add_call_register_instruction: Op::CALLR,
            add_dfr_instruction: Op::DFR,
            add_dfi_instruction: Op::DFI,
            add_lzy_instruction: Op::LZY,
            add_paus_instruction: Op::PAUS,
            add_psh_instruction: Op::PSH,
            add_pop_instruction: Op::POP
        );

        generate_binary_instruction!(
            add_add_instruction: Op::ADD,
            add_sub_instruction: Op::SUB,
            add_mul_instruction: Op::MUL,
            add_div_instruction: Op::DIV,
            add_shr_instruction: Op::SHR,
            add_shl_instruction: Op::SHL,
            add_and_instruction: Op::AND,
            add_or_instruction: Op::OR,
            add_xor_instruction: Op::XOR,
            add_band_instruction: Op::BAND,
            add_bor_instruction: Op::BOR,
            add_bxor_instruction: Op::BXOR,
            add_land_instruction: Op::LAND,
            add_lor_instruction: Op::LOR,
            add_lxor_instruction: Op::LXOR,
            add_eq_instruction: Op::EQ,
            add_beq_instruction: Op::BEQ,
            add_ieq_instruction: Op::IEQ,
            add_neq_instruction: Op::NEQ,
            add_bneq_instruction: Op::BNEQ,
            add_ineq_instruction: Op::INEQ,
            add_copy_instruction: Op::COPY,
            add_cir_instruction: Op::CIR,
            add_cbr_instruction: Op::CBR,
            add_ceq_instruction: Op::CEQ,
            add_lodb_instruction: Op::LODB,
            add_lodi_instruction: Op::LODI,
            add_ret_instruction: Op::RET
        );
    };
}

#[derive(Debug)]
pub struct FrameBuilder<T: VMValue<Op, Register, T>> {
    pub instructions: Vec<u8>,
    pub pc: usize,
    pub locals: IndexMap<String, FrameValue<T>>,
    pub parent: Option<usize>,
}

impl <T: VMValue<Op, Register, T>> Default for FrameBuilder<T> {
    #[inline]
    fn default() -> Self {
        FrameBuilder {
            instructions: vec![],
            pc: 0,
            locals: Default::default(),
            parent: None,
        }
    }
}

impl <T: VMValue<Op, Register, T>> FrameBuilder<T> {
    #[inline]
    pub fn new() -> FrameBuilder<T> {
        Self::default()
    }

    fn extend_instructions(&mut self, instructions: Vec<u8>) -> &mut Self {
        self.instructions.extend(instructions);
        self
    }

    fn extend_instructions_raw(&mut self, instructions: &[u8]) -> &mut Self {
        self.instructions.extend(instructions);
        self
    }

    pub fn store_register(&mut self, register: Register) -> &mut Self {
        self.instructions.extend(length_to_bytes(&register.into()));
        self
    }

    common_instructions!();

    pub fn build(&mut self) -> Frame<T> {
        Frame {
            instructions: mem::take(&mut self.instructions),
            pc: self.pc,
            locals: mem::take(&mut self.locals),
            parent: self.parent.take()
        }
    }
}

impl <T: VMValue<Op, Register, T>> Default for VMBuilder<T> {
    fn default() -> Self {
        VMBuilder {
            registers: HashMap::new(),
            index_registers: HashMap::new(),
            fp: 0,
            functions: vec![],
            hcf_trigger: None,
            lazy_compiler: None,
            frames: vec![
                Frame::new()
            ],
            bit_registers: HashMap::new()
        }
    }
}


impl <T: VMValue<Op, Register, T>> VMBuilder<T> {

    #[inline]
    pub fn new() -> VMBuilder<T> {
        Self::default()
    }

    pub fn set_value(&mut self, register: Register, value: T) -> &mut Self {
        if register.index() != 0 {
            self.registers.insert(register, value);
        }
        self
    }

    pub fn set_index(&mut self, register: Register, value: Length) -> &mut Self {
        if register.index() != 0 {
            self.index_registers.insert(register, value);
        }
        self
    }

    fn extend_instructions(&mut self, instructions: Vec<u8>) -> &mut Self {
        self.frames[self.fp].instructions.extend(instructions);
        self
    }

    fn extend_instructions_raw(&mut self, instructions: &[u8]) -> &mut Self {
        self.frames[self.fp].instructions.extend(instructions);
        self
    }

    common_instructions!();

    pub fn add_function(&mut self, func: VMFunction<T>) -> &mut Self {
        self.functions.push(func);
        self
    }

    pub fn with_hcf_function(&mut self, func: HCFFunction<T>) -> &mut Self {
        self.hcf_trigger = Some(func);
        self
    }

    pub fn with_lazy_compiler(&mut self, func: Box<dyn LazyCompiler<T>>) -> &mut Self {
        self.lazy_compiler = Some(func);
        self
    }

    pub fn store_register(&mut self, register: Register) -> &mut Self {
        self.frames[self.fp].instructions.extend(length_to_bytes(&register.into()));
        self
    }

    pub fn enter_frame(&mut self, frame: Frame<T>) -> &mut Self {
        self.frames.push(frame);
        self.fp += 1;
        self
    }

    pub fn exit_frame(&mut self, from: Register, result: Register) -> &mut Self {
        self.add_ret_instruction(from, result);
        self.fp -= 1;
        self
    }

    pub fn build(&mut self) -> RegisterVM<T> {
        RegisterVM {
            fp: 0,
            functions: mem::take(&mut self.functions),
            index_registers: mem::take(&mut self.index_registers),
            registers: mem::take(&mut self.registers),
            stack: vec![],
            hcf_trigger: self.hcf_trigger.take(),
            lazy_compiler: self.lazy_compiler.take(),
            frames: mem::take(&mut self.frames),
            bit_registers: mem::take(&mut self.bit_registers),
            value_stack: vec![],
        }
    }
}