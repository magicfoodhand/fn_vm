use std::collections::HashMap;
use std::mem;
use indexmap::IndexMap;
use small_len::{Len, Length};
use vm_value::{VM, VMStatus, VMValue};
use crate::{Op, VMError};
use crate::builder::Register;

pub type VMFunction<T> = fn(&mut RegisterVM<T>, args: Vec<T>) -> Result<Option<T>, VMError>;
pub type HCFFunction<T> = fn(&mut RegisterVM<T>) -> Result<(), VMError>;


pub trait LazyCompiler<T: VMValue<Op, Register, T>> {
    fn compile(&self, length: Length) -> Result<Frame<T>, VMError>;
}

#[derive(Clone, Debug, PartialEq)]
pub enum FrameValue<T: VMValue<Op, Register, T>> {
    Let(T),
    Mut(T),
    Frame(Length),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Frame<T: VMValue<Op, Register, T>> {
    pub instructions: Vec<u8>,
    pub pc: usize,
    pub locals: IndexMap<String, FrameValue<T>>,
    pub parent: Option<usize>,
}

impl <T: VMValue<Op, Register, T>> Default for Frame<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl <T: VMValue<Op, Register, T>> Frame<T> {
    pub fn new() -> Self {
        Frame {
            instructions: vec![],
            pc: 0,
            locals: IndexMap::new(),
            parent: None,
        }
    }

    pub fn child(parent: usize) -> Self {
        Frame {
            instructions: vec![],
            pc: 0,
            locals: IndexMap::new(),
            parent: Some(parent),
        }
    }

    pub fn reset(&mut self) {
        self.locals = IndexMap::new();
        self.pc = 0
    }
}

pub struct StackValue<T: VMValue<Op, Register, T>> {
    pub pc: usize,
    pub bp: usize,
    pub locals: IndexMap<String, FrameValue<T>>,
}

pub struct RegisterVM<T: VMValue<Op, Register, T>> {
    pub fp: usize,
    pub frames: Vec<Frame<T>>,
    pub functions: Vec<VMFunction<T>>,
    pub registers: HashMap<Length, T>,
    pub index_registers: HashMap<Length, Length>,
    pub bit_registers: HashMap<Length, bool>,
    pub stack: Vec<StackValue<T>>,
    pub value_stack: Vec<T>,
    pub lazy_compiler: Option<Box<dyn LazyCompiler<T>>>,
    pub hcf_trigger: Option<HCFFunction<T>>,
}

impl <T: VMValue<Op, Register, T>> VM<Op, Register, T> for RegisterVM<T> {

    type Error = VMError;

    fn run(&mut self) -> Result<VMStatus<Op, Register>, VMError> {
        let mut op = Op::IVD;
        loop {
            if self.pc() >= self.instructions().len() {
                break
            }
            let pc = self.pc();
            let raw = self.instruction(pc);
            op = Op::from(raw);
            let pc = self.pc_set(pc + 1);
            match op {
                Op::NOP => {
                    self.pc_set(pc + 1);
                }
                Op::COPY => {
                    let source = self.next_len();
                    let dest = self.next_len();
                    let value = match self.registers.get(&source) {
                        None => return Err(VMError::RegisterNotFound(format!("Register {} is empty", source.index()))),
                        Some(v) => v.clone(),
                    };
                    self.set_register_value(dest, value);
                }
                Op::CIR => {
                    let source = self.next_len();
                    let dest = self.next_len();
                    let value = match self.index_registers.get(&source) {
                        None => return Err(VMError::RegisterNotFound(format!("Index Register {} is empty", source.index()))),
                        Some(v) => *v,
                    };
                    self.set_index_register_value(dest, value);
                }
                Op::LOAD => {
                    let dest = self.next_len();
                    let bytes = T::create_from_vm_bytes(self);
                    self.set_register_value(dest, bytes);
                }
                Op::LODB => {
                    let dest = self.next_len();
                    let value = self.next_byte() != 0;
                    self.set_bit_register_value(dest, value);
                }
                Op::LODI => {
                    let dest = self.next_len();
                    let value = self.next_len();
                    self.set_index_register_value(dest, value);
                }
                Op::CBR => {
                    let source = self.next_len();
                    let dest = self.next_len();
                    let value = match self.bit_registers.get(&source) {
                        None => return Err(VMError::RegisterNotFound(format!("Bit Register {} is empty", source.index()))),
                        Some(v) => *v,
                    };
                    self.set_bit_register_value(dest, value);
                }
                Op::FN => {
                    let command = self.next_len().index();
                    if self.functions.len() <= command {
                        return Err(VMError::InvalidFunction(format!("Invalid function: {}", command)));
                    }

                    let args_len = self.next_len().index();
                    let output_register = self.next_len();

                    let mut args = vec![];
                    for _ in 0..args_len {
                        let r = self.next_len();
                        args.push(self.remove_register(&r)?);
                    }
                    let function = self.functions[command].to_owned();
                    let result = function(self, args)?;
                    if let Some(result) = result {
                        self.set_register_value(output_register, result);
                    }
                }
                Op::CALL => {
                    let frame = self.next_len();
                    self.call_frame(frame)?;
                }
                Op::CALLR => {
                    let index = self.next_len();
                    let frame = self.remove_index_register(&index)?;
                    self.call_frame(frame)?;
                }
                Op::CEQ => {
                    let b = self.next_len();
                    let ifr = self.next_len();
                    let elser = self.next_len();
                    if self.remove_bit_register(&b)? {
                        if ifr != 0.into() {
                            self.call_frame(ifr)?;
                        }
                    } else if elser != 0.into() {
                        self.call_frame(elser)?;
                    }
                }
                Op::RET => {
                    let from = self.next_len();
                    let to = self.next_len();
                    let value = self.remove_register(&from)?;
                    self.set_register_value(to, value);
                    match self.frames.get_mut(self.fp) {
                        None => return Err(VMError::InvalidFrame(format!("Frame does not exist: {}", self.fp))),
                        Some(f) => f.reset()
                    };
                    let s = match self.stack.pop() {
                        None => {
                            return Err(VMError::StackUnderflow("Attempted to return with empty stack".into()))
                        }
                        Some(f) => f,
                    };

                    let StackValue { pc, bp, locals } = s;
                    {
                        self.frames[bp].locals = locals;
                        self.frames[bp].pc = pc;
                        self.fp = bp;
                    }
                }
                Op::GLV => {
                    let register = self.next_len();
                    let var = self.next_str();
                    let value = self.get_local(self.fp, &var)?;
                    self.set_register_value(register, value);
                }
                Op::MVL => {
                    let frame = self.next_len();
                    let var = self.next_str();
                    match self.frames[self.fp].locals.shift_remove(&var) {
                        None => return Err(VMError::LocalNotFound(format!("{} does not exist in current frame", var))),
                        Some(v) => {
                            self.set_local_value(frame.index(), var, v)?;
                        }
                    }
                }
                Op::CPL => {
                    let frame = self.next_len();
                    let var = self.next_str();
                    let value = self.get_local_raw(self.fp, &var)?;
                    self.set_local_value(frame.index(), var, value)?;
                }
                Op::CPM => {
                    let frame = self.next_len();
                    let var = self.next_str();
                    let value = self.get_local_raw(self.fp, &var)?;
                    self.set_local_value(frame.index(), var, value)?;
                }
                Op::DLV => {
                    let frame = self.next_len();
                    let var = self.next_str();
                    self.frames[frame].locals.shift_remove(&var);
                }
                Op::DFV => {
                    let var = self.next_str();
                    self.frames[self.fp].locals.shift_remove(&var);
                }
                Op::SLF => {
                    let fr = self.next_len();
                    let var = self.next_str();
                    let frame = self.remove_index_register(&fr)?;
                    self.set_local_frame(self.fp, var, frame)?
                }
                Op::SLR => {
                    let register = self.next_len();
                    let var = self.next_str();
                    let value = self.remove_register(&register)?;
                    self.set_local(self.fp, var, value)?;
                }
                Op::SMR => {
                    let register = self.next_len();
                    let var = self.next_str();
                    let value = self.remove_register(&register)?;
                    self.set_mutable_local(self.fp, var, value)?;
                }
                Op::CFR => {
                    let register = self.next_len();
                    let mut f = Frame::child(self.fp);
                    let len = self.next_len();
                    let next = self.next_n_bytes_vec(len);
                    f.instructions.extend(next);
                    self.frames.push(f);
                    self.set_index_register_value(register, self.frames.length() - 1);
                }
                Op::DFR => {
                    let index = self.next_len().index();
                    match self.frames.get_mut(index) {
                        None => return Err(VMError::InvalidFrame(format!("Frame does not exist: {}", index))),
                        Some(f) => {
                            f.instructions = vec![Op::IVF as u8];
                            f.reset();
                        }
                    }
                }
                Op::DFI => {
                    let ir = self.next_len();
                    let index = self.remove_index_register(&ir)?.index();
                    self.frames.remove(index);
                }
                Op::LZY => {
                    let len = self.next_len();
                    let lazy = match &self.lazy_compiler {
                        None => return Err(VMError::LazyDisabled("`lazy_compiler` not set".into())),
                        Some(j) => j
                    };

                    let frame = lazy.compile(len)?;
                    self.frames.push(frame);
                    self.call_frame(self.frames.length() - 1)?;
                }
                Op::HCF => {
                    if let Some(hcf) = self.hcf_trigger {
                        hcf(self)?;
                    }

                    break
                },
                Op::IVF => return Err(VMError::InvalidFrame(format!("Invalid frame: {}", self.fp))),
                Op::IVD => return Err(VMError::InvalidInstruction(format!("Invalid instruction: {}", raw))),
                Op::ADD => self.binary_expression(T::add)?,
                Op::MOD => self.binary_expression(T::rem)?,
                Op::SUB => self.binary_expression(T::sub)?,
                Op::MUL => self.binary_expression(T::mul)?,
                Op::DIV => self.binary_expression(T::div)?,
                Op::SHR => self.binary_expression(T::shr)?,
                Op::SHL => self.binary_expression(T::shl)?,
                Op::NOT => self.unary_expression(T::not)?,
                Op::REV => self.unary_expression_ptr(T::bitwise_reverse)?,
                Op::BNOT => self.unary_expression_bool(T::bool_not)?,
                Op::NEG => self.unary_expression(T::neg)?,
                Op::AND => self.binary_expression(T::bitand)?,
                Op::OR => self.binary_expression(T::bitor)?,
                Op::XOR => self.binary_expression(T::bitxor)?,
                Op::BXOR => self.binary_expression_bool(T::bool_xor)?,
                Op::BAND => self.binary_expression_bool(T::bool_and)?,
                Op::BOR => self.binary_expression_bool(T::bool_or)?,
                Op::LXOR => self.binary_expression_ptr(T::logical_xor)?,
                Op::LAND => self.binary_expression_ptr(T::logical_and)?,
                Op::LOR => self.binary_expression_ptr(T::logical_or)?,
                Op::EQ => self.binary_expression_bool(T::eq)?,
                Op::BEQ => self.binary_expression_bits(bool::eq)?,
                Op::IEQ => self.binary_expression_index(Length::eq)?,
                Op::NEQ => self.binary_expression_bool(T::ne)?,
                Op::BNEQ => self.binary_expression_bits(bool::ne)?,
                Op::INEQ => self.binary_expression_index(Length::ne)?,
                Op::LT => self.binary_expression_bool(T::lt)?,
                Op::LTE => self.binary_expression_bool(T::lt)?,
                Op::GT => self.binary_expression_bool(T::gt)?,
                Op::GTE => self.binary_expression_bool(T::ge)?,
                Op::PAUS => {
                    let len = self.next_len();
                    let value = if len.index() > 0 {
                        Some(len)
                    } else {
                        None
                    };
                    return Ok(VMStatus { done: false, last_command: op, value })
                },
                Op::PSH => {
                    let register = self.next_len();
                    let v = self.remove_register(&register)?;
                    self.value_stack.push(v);
                }
                Op::PSHV => {
                    let t = T::create_from_vm_bytes(self);
                    self.value_stack.push(t);
                }
                Op::POP => {
                    match self.value_stack.pop() {
                        None => return Err(VMError::StackUnderflow("value_stack was empty".into())),
                        Some(v) => {
                            let register = self.next_len();
                            self.set_register_value(register, v);
                        }
                    };
                }
            }
        }
        Ok(VMStatus { done: true, last_command: op, value: None })
    }

    fn execute(&mut self) -> Result<(), VMError> {
        loop {
            match self.run() {
                Ok(ok) => {
                    if ok.done {
                        break;
                    }
                }
                Err(e) => return Err(e)
            }
        }
        Ok(())
    }
    
    fn next_byte(&mut self) -> u8 {
        let pc = self.pc();
        let result = self.instruction(pc);
        self.pc_set(pc + 1);
        result
    }

    fn next_n_bytes<const N: usize>(&mut self) -> [u8; N] {
        let mut result = [0; N];
        let pc = self.pc();
        result.copy_from_slice(&self.instructions()[pc..pc + N]);
        self.pc_set(pc + N);
        result
    }

    fn next_n_bytes_vec(&mut self, n: Length) -> Vec<u8> {
        let pc = self.pc();
        let end = (pc + n).index();
        let result = self.instructions()[pc..end].to_vec();
        self.pc_set(end);
        result
    }
}

impl <T: VMValue<Op, Register, T>> RegisterVM<T> {
    
    pub fn empty() -> RegisterVM<T> {
        RegisterVM {
            fp: 0,
            frames: vec![],
            stack: vec![],
            functions: vec![],
            registers: HashMap::new(),
            index_registers: HashMap::new(),
            bit_registers: HashMap::new(),
            hcf_trigger: None,
            lazy_compiler: None,
            value_stack: vec![],
        }
    }

    pub fn pc(&mut self) -> usize {
        self.frames[self.fp].pc
    }

    pub fn pc_set(&mut self, new: usize) -> usize {
        self.frames[self.fp].pc = new;
        new
    }

    pub fn instructions(&mut self) -> &Vec<u8> {
        &self.frames[self.fp].instructions
    }

    pub fn instruction(&mut self, index: usize) -> u8 {
        self.frames[self.fp].instructions[index]
    }

    pub fn get_local_raw(&mut self, fp: usize, var: &String) -> Result<FrameValue<T>, VMError> {
        match self.frames[fp].locals.get(var) {
            None => {
                match self.frames[fp].parent {
                    None => {
                        Err(VMError::LocalNotFound(format!("{} not found", var)))
                    }
                    Some(parent) => self.get_local_raw(parent, var)
                }
            }
            Some(local) => {
                Ok(local.clone())
            }
        }
    }

    pub fn get_local(&mut self, fp: usize, var: &String) -> Result<T, VMError> {
        let local = self.get_local_raw(fp, var)?;
        let f = match local {
            FrameValue::Let(v) | FrameValue::Mut(v) => {
                return Ok(v.clone())
            }
            FrameValue::Frame(f) => {
                f
            }
        };

        self.call_frame(f)?;
        let VMStatus {  value, .. } = self.run()?;
        match value {
            None => Err(VMError::LocalNotFound("Frame Evaluation Failed".to_string())),
            Some(v) => self.remove_register(&v)
        }
    }

    pub fn local_exists(&mut self, fp: usize, var: &String) -> (bool, usize) {
        if self.frames[fp].locals.contains_key(var) {
            return (true, fp)
        }

        match self.frames[fp].parent {
            None => (false, 0),
            Some(parent) => self.local_exists(parent, var)
        }
    }

    pub fn set_local(&mut self, fp: usize, var: String, value: T) -> Result<(), VMError> {
        let (exists, _) = self.local_exists(fp, &var);
        if exists {
            return Err(VMError::ImmutableLocal(format!("{} is immutable", var)))
        }
        self.frames[fp].locals.insert(var, FrameValue::Let(value));
        Ok(())
    }

    pub fn set_mutable_local(&mut self, fp: usize, var: String, value: T) -> Result<(), VMError> {
        let (exists, parent) = self.local_exists(fp, &var);
        let fp = if exists {
            parent
        } else {
            fp
        };

        let v = self.frames[fp].locals.insert(var.clone(), FrameValue::Mut(value));

        if let Some(FrameValue::Let(_)) = v {
            return Err(VMError::ImmutableLocal(format!("Attempted to update immutable variable: {}", var)))
        }

        Ok(())
    }

    pub fn set_local_frame(&mut self, fp: usize, var: String, value: Register) -> Result<(), VMError> {
        self.set_local_value(fp, var, FrameValue::Frame(value))
    }

    pub fn set_local_value(&mut self, fp: usize, var: String, value: FrameValue<T>) -> Result<(), VMError> {
        self.frames[fp].locals.insert(var, value);
        Ok(())
    }

    pub fn remove_register(&mut self, index: &Length) -> Result<T, VMError> {
        match self.registers.remove(index) {
            None => Err(VMError::RegisterNotFound(format!("register {} not found", index))),
            Some(l) => Ok(l)
        }
    }

    pub fn remove_index_register(&mut self, index: &Length) -> Result<Length, VMError> {
        match self.index_registers.remove(index) {
            None => Err(VMError::RegisterNotFound(format!("index_register {} not found", index))),
            Some(l) => Ok(l)
        }
    }

    pub fn remove_bit_register(&mut self, index: &Length) -> Result<bool, VMError> {
        match self.bit_registers.remove(index) {
            None => Err(VMError::RegisterNotFound(format!("bit_register {} not found", index))),
            Some(l) => Ok(l)
        }
    }

    pub fn unary_expression(&mut self, operation: fn(T) -> T) -> Result<(), VMError> {
        let o = self.next_len();
        let a = self.remove_register(&o)?;
        self.set_register_value(o, operation(a));
        Ok(())
    }

    pub fn unary_expression_ptr(&mut self, operation: fn(&T) -> T) -> Result<(), VMError> {
        let o = self.next_len();
        let a = self.remove_register(&o)?;
        self.set_register_value(o, operation(&a));
        Ok(())
    }

    pub fn unary_expression_bool(&mut self, operation: fn(&T) -> bool) -> Result<(), VMError> {
        let o = self.next_len();
        let a = self.remove_register(&o)?;
        self.set_bit_register_value(o, operation(&a));
        Ok(())
    }

    pub fn binary_expression(&mut self, operation: fn(T, T) -> T) -> Result<(), VMError> {
        let o = self.next_len();
        let b = self.next_len();
        let a = self.remove_register(&o)?;
        let b = self.remove_register(&b)?;
        self.set_register_value(o, operation(a, b));
        Ok(())
    }

    pub fn binary_expression_bool(&mut self, operation: fn(&T, &T) -> bool) -> Result<(), VMError> {
        let o = self.next_len();
        let b = self.next_len();
        let a = self.remove_register(&o)?;
        let b = self.remove_register(&b)?;
        self.set_bit_register_value(o, operation(&a, &b));
        Ok(())
    }

    pub fn binary_expression_bits(&mut self, operation: fn(&bool, &bool) -> bool) -> Result<(), VMError> {
        let o = self.next_len();
        let b = self.next_len();
        let a = self.remove_bit_register(&o)?;
        let b = self.remove_bit_register(&b)?;
        self.set_bit_register_value(o, operation(&a, &b));
        Ok(())
    }

    pub fn binary_expression_index(&mut self, operation: fn(&Register, &Register) -> bool) -> Result<(), VMError> {
        let o = self.next_len();
        let b = self.next_len();
        let a = self.remove_index_register(&o)?;
        let b = self.remove_index_register(&b)?;
        self.set_bit_register_value(o, operation(&a, &b));
        Ok(())
    }

    pub fn binary_expression_ptr(&mut self, operation: fn(&T, &T) -> T) -> Result<(), VMError> {
        let o = self.next_len();
        let b = self.next_len();
        let a = self.remove_register(&o)?;
        let b = self.remove_register(&b)?;
        self.set_register_value(o, operation(&a, &b));
        Ok(())
    }

    pub fn call_frame(&mut self, frame: Register) -> Result<(), VMError> {
        let current = &mut self.frames[self.fp];
        let locals = mem::take(&mut current.locals);
        self.stack.push(StackValue {
            pc: current.pc,
            locals,
            bp: self.fp,
        });
        self.frames[self.fp].reset();
        self.fp = frame.index();
        Ok(())
    }

    pub fn set_register_value(&mut self, register: Length, value: T) -> &mut Self {
        if register == 0.into() {
            return self
        }
        self.registers.insert(register, value);
        self
    }

    pub fn set_index_register_value(&mut self, register: Length, value: Length) -> &mut Self {
        if register == 0.into() {
            return self
        }
        self.index_registers.insert(register, value);
        self
    }

    pub fn set_bit_register_value(&mut self, register: Length, value: bool) -> &mut Self {
        if register == 0.into() {
            return self
        }
        self.bit_registers.insert(register, value);
        self
    }
}